import React from 'react';
import PokemonCard from '../Components/Pokemon/PokemonCard';
import PokemonTable from '../Components/Pokemon/PokemonTable';
import PokemonFilters from '../Components/Pokemon/PokemonFilters';

export default class PokemonContainer extends React.Component {
  render() {
    return (
      <div className="flex ">
        <div className="w-1/4 mx-6 bg-green-lightest">
          <PokemonFilters />
        </div>
        <div className="w-3/4 mx-6 bg-green">
          <PokemonCard />
        </div>
      </div>
    );
  }
}
