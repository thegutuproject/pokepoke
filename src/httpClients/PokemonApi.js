import axios from 'axios';

const api = axios.create({
  baseURL: 'https://pokeapi.co/api/v2',
  method: 'get',
  timeout: 1000,
  headers: {
    'Access-Control-Allow-Origin': '*'
  },
  responseType: 'json'
});

const POKEMON_API = 'https://pokeapi.co/api/v2';

export default class PokemonApi {
  /**
   * GET /api/v2/pokemon/{id or name}/
   *
   * Pokémon are the creatures that inhabit the world
   * of the Pokémon games. They can be caught using Pokéballs
   * and trained by battling with other Pokémon. See
   * Bulbapedia for greater detail.
   * @returns {Promise<AxiosResponse<any> | never>}
   */

  getAllPokemon = () => {
    const url = POKEMON_API + '/pokemon/';
    return axios.get(url).then((response) => response.data);
  };

  /**
   * GET /api/v2/version/{id or name}/
   *
   * Versions of the games, e.g., Red, Blue or Yellow.
   * @returns {Promise<AxiosResponse<any> | never>}
   */
  getAllPokemonVersions = () => {
    const url = POKEMON_API + '/version';
    return axios.get(url).then((response) => response.data);
  };

  /**
   * GET /api/v2/generation/{id or name}/
   *
   * A generation is a grouping of the Pokémon games that
   * separates them based on the Pokémon they include. In
   * each generation, a new set of Pokémon, Moves, Abilities
   * and Types that did not exist in the previous generation
   * are released.
   * @returns {Promise<AxiosResponse<any> | never>}
   */
  getAllPokemonGenerations = () => {
    const url = POKEMON_API + '/generation';
    return axios.get(url).then((response) => response.data);
  };

  /**
   * GET /api/v2/version-group/{id or name}/
   *
   * Version groups categorize highly similar versions of the games.
   */
  getAllPokemonGameGroups = () => {
    const url = POKEMON_API + '/version-group';
    return axios.get(url).then((response) => response.data);
  };
}
