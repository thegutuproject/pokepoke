import React from 'react';
import { inject, observer } from 'mobx-react';

class PokemonCard extends React.Component {
  render() {
    let { pokemonStore } = this.props.rootStore;
    console.log(pokemonStore.pokemon);
    return (
      <div>
        <ul>
          {pokemonStore.pokemon.map((pokemon, index) => {
            return (
              <li>
                <a key={index} href={pokemon.url}>
                  {pokemon.name}
                </a>
              </li>
            );
          })}
        </ul>
      </div>
    );
  }
}

export default inject('rootStore')(observer(PokemonCard));
