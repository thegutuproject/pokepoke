import React from 'react';
import ReactDataGrid from 'react-data-grid';
import { inject, observer } from 'mobx-react';

const columns = [
  {
    key: 'name',
    name: 'Name'
  },
  {
    key: 'url',
    name: 'URL'
  }
];

class PokemonTable extends React.Component {
  render() {
    const { pokemonStore } = this.props.rootStore;

    return <div>React Data Grid</div>;
  }
}

export default inject('rootStore')(observer(PokemonTable));
