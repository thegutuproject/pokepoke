import React from 'react';
import Link from 'react-router-dom';

class NavItem extends React.Component {
  render() {
    return (
      <li className="flex-1 mr-2">
        <Link
          className="text-center block border border-blue rounded py-2 px-4 bg-blue hover:bg-blue-dark text-white"
          href="#">
          Active Item
        </Link>
      </li>
    );
  }
}

export default NavItem;
