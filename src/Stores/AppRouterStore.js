import { RouterStore } from 'mobx-react-router';

export default class AppRouteStore extends RouterStore {
  get isHomeTab() {
    return this.location.pathname === '/';
  }
  get isAboutTab() {
    return this.location.pathname === '/about';
  }
  getPathParams(path) {
    let pathSplit = path.split('/');
    let urlSplit = this.location.pathname.split('/');
    let values = {};
    pathSplit.forEach((i, index) => {
      if (i.includes(':')) {
        let key = i.substr(1);
        values[key] = urlSplit[index];
      }
    });
    return values;
  }

  // decorate(AppRouteStore, {
  //   isHomeTab: computed,
  //   isAboutTab: computed
  // })
}
