import PokemonApi from '../httpClients/PokemonApi';
import AppRouterStore from './AppRouterStore';
import PokemonStore from './PokemonStore';

export default class RootStore {
  constructor() {
    this.registerApis();
    this.registerStores();
  }

  registerApis() {
    this.pokemonApi = new PokemonApi();
  }

  registerStores() {
    this.appRouteStore = new AppRouterStore();
    this.pokemonStore = new PokemonStore(this.pokemonApi);
  }
}
