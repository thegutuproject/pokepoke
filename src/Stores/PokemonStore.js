import { action, extendObservable, autorun } from 'mobx';

export default class PokemonStore {
  constructor(pokemonApi) {
    this.pokemonApi = pokemonApi;

    this.defaults = {
      count: 0,
      pokemon: []
    };

    extendObservable(this, {
      pokemon: this.defaults['pokemon'],
      count: this.defaults['coount'],
      setPokemon: action((pokemon) => {
        this.pokemon = pokemon;
      }),
      setCount: action((count) => {
        this.count = count;
      })
    });

    // things you want run when data changes
    autorun(() => {
      this.loadPokemon();
    });
  }

  loadPokemon() {
    this.pokemonApi.getAllPokemon().then((response) => {
      this.setPokemon(Array.from(response.results));
      this.setCount(response.count);
    });
  }

  get pokemon() {
    return this.pokemon;
  }
}

// computed are things you built based on data
