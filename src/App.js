import React, { Component } from 'react';
import RootStore from './Stores/RootStore';
import { Provider, observer } from 'mobx-react';
import PokemonContainer from './Containers/PokemonContainer';
import './App.css';
import './css/tailwind.css';

const rootStore = new RootStore();

class App extends Component {
  render() {
    return (
      <Provider rootStore={rootStore}>
        <div>
          <div className="text-center">
            <header className="bg-purple m-6 p-6 rounded shadow-lg">
              <h1 className="text-white text-3xl">Welcome to React!</h1>
            </header>
          </div>
          {/*<div className="container mx-auto">*/}
          <div>
            <PokemonContainer />
          </div>
          {/*</div>*/}
        </div>
      </Provider>
    );
  }
}
export default observer(App);
